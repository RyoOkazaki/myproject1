﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreRanking : SavableSingletonBase<ScoreRanking>
{
    [SerializeField] List<QuestResultData> resultDataList = new List<QuestResultData>(5);
    private const int rankingSize = 5;
    public List<QuestResultData> ScoreRankingList
    {
        get
        {
            if(resultDataList.Count == 0)
            {
                QuestResultData resultData = new QuestResultData(PlayerData.PlayerRank.Low,0,0,0,0);

                for(int i = 0; i < rankingSize; i++)
                {
                    resultDataList.Add(resultData);
                }
            }
            return resultDataList;
        }
    }

    public void SortArray(QuestResultData resultData)
    {
        if(resultData._totalScore > ScoreRankingList[0]._totalScore)
        {
            resultDataList.RemoveAt(rankingSize - 1);
            resultDataList.Insert(0,resultData);
            Save();
        }
    }
}

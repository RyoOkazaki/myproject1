﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TutorialStarter : MonoBehaviour
{
    [SerializeField] Canvas tutorialCanvas;
    public TutorialData tutorialData;
    [SerializeField] Image tutorialImage;
    [SerializeField] Button tutorialNexter;
    int index = 0;
    bool isInit = false;
    int Count { get { return tutorialData.tutorialParts.Count; } }
    TutorialData.TutorialPart CurrentTutorial
    {
        get
        {
            return tutorialData.tutorialParts[index];
        }
    }

    public TutorialEvent tutorialEvents = new TutorialEvent();

    void Awake()
    {
        if(isInit)
        {
            return;
        }
        tutorialCanvas.worldCamera = Camera.main;
        tutorialCanvas.gameObject.SetActive(false);
        isInit = true;
    }

    public void StartTutorial()
    {
        if(!isInit)
        {
            Awake();
        }
        tutorialCanvas.gameObject.SetActive(true);
        index = 0;
        TutorialPlay();
    }
    public void OnTouched()
    {
        if(index < Count)
        {
            TutorialPlay();
        }
        else
        {
            tutorialCanvas.gameObject.SetActive(false);
            tutorialEvents.Invoke(tutorialData.tag,index,Timing.Exit);
        }
    }

    void TutorialPlay()
    {
        tutorialImage.sprite = Getsprite;
        if(CurrentTutorial.isEvent)
        {
            tutorialEvents.Invoke(tutorialData.tag,index,Timing.EveryTime);

            if(index == 0)
            {
                tutorialEvents.Invoke(tutorialData.tag,index,Timing.First);
            }
            if(index == Count - 1)
            {
                tutorialEvents.Invoke(tutorialData.tag,index,Timing.Last);
            }
        }

        index++;
    }

    Sprite Getsprite
    {
        get
        {
            return tutorialData.tutorialParts[index].sprite;
        }
    }

    public void SetInteractable(bool isInteractable)
    {
        tutorialNexter.interactable = isInteractable;
    }
    public void SetActive(bool _isActive)
    {
        if(tutorialCanvas.gameObject.activeSelf == false
            && _isActive == true)
        {
            isInit = true;
        }
        tutorialCanvas.gameObject.SetActive(_isActive);

    }


    public class TutorialEvent : UnityEvent<Progress.Tutorial,int,Timing>
    {
        public TutorialEvent() { }
    }
    public void EventsReset()
    {
        tutorialEvents.RemoveAllListeners();
    }

    public enum Timing
    {
        First,
        EveryTime,
        Last,
        Exit,
    }
}
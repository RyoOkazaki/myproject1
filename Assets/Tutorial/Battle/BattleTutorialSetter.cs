﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BattleTutorialSetter : MonoBehaviour
{
    [SerializeField] TutorialStarter tutorialStarter;
    [SerializeField] TutorialData tutorialData;
    Progress progress;
    void Awake()
    {
        progress = Progress.Instance;
    }

    void Start()
    {
        if(!progress.UnlockedTutorial.HasFlag(Progress.Tutorial.Battle))
        {
            tutorialStarter.tutorialData = tutorialData;
            StartTutorial(Progress.Tutorial.Battle);
        }
    }

    /// <summary>チュートリアルを再開する</summary>
    public void ResumeTutorial()
    {
        Time.timeScale = 0;
        tutorialStarter.SetActive(true);
        tutorialStarter.OnTouched();
    }

    /// <summary>チュートリアルを開始する</summary>
    /// <param name="tutorial"></param>
    public void StartTutorial(Progress.Tutorial tutorial)
    {
        Time.timeScale = 0;
        tutorialStarter.tutorialEvents.AddListener((tag,index,timing) =>
        {
            if(tag == tutorial)
            {
                if(timing == TutorialStarter.Timing.Exit)
                {
                    progress.FlagTutorial(tutorial);
                }
                switch(tag)
                {
                    case Progress.Tutorial.Battle:
                        switch(index)
                        {
                            case 1:
                                tutorialStarter.SetActive(false);
                                Time.timeScale = 1f;
                                break;
                            case 3:
                                tutorialStarter.SetActive(false);
                                Time.timeScale = 1f;
                                progress.FlagTutorial(tutorial);
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }

            }
        });

        tutorialStarter.StartTutorial();
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable, CreateAssetMenu(fileName = "TutorialData",menuName = "TutorialData")]
public class TutorialData : ScriptableObject
{
    [Header("")] public Progress.Tutorial tag;
    public List<TutorialPart> tutorialParts = new List<TutorialPart>();

    [Serializable]
    public class TutorialPart
    {
        [Header("表示する画像")] public Sprite sprite;
        [Header("画像表示後に何かしらのEventがあるか")] public bool isEvent;
    }

}

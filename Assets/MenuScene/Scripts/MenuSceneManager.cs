﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
public class MenuSceneManager : MonoBehaviour
{
    Progress progress;
    [SerializeField] List<Button> questSelectButtons = new List<Button>();
    [SerializeField] Button toTitleButton = null;
    [SerializeField] Button rankingButton = null;
    [SerializeField] GameObject rankingBoard = null;
    [SerializeField] GameObject playerRankBoard = null;
    [SerializeField] List<Button> windowCloseButton = new List<Button>();
    [SerializeField] Button selectPlayerRankButton = null;
    [SerializeField] List<Button> playerRankButtons = null;
    [SerializeField] List<GameObject> currentSelectIcons = new List<GameObject>();
    [SerializeField] List<Text> playerRankTexts = new List<Text>(5);
    [SerializeField] List<Text> playerHpTexts = new List<Text>(5);
    [SerializeField] List<Text> clearTimeTexts = new List<Text>(5);
    [SerializeField] List<Text> enemykillTexts = new List<Text>(5);
    [SerializeField] List<Text> scoreTexts = new List<Text>(5);

    SoundManager soundManager;
    private void Awake()
    {
        progress = Progress.Instance;
        soundManager = SoundManager.Instance;
    }

    private void Start()
    {
        playerRankBoard.SetActive(false);
        rankingBoard.SetActive(false);
        int index = 0;
        for(int i = 1; i <= (int)QuestData.Difficulty.Level4; i <<= 1)
        {
            QuestData.Difficulty difficulty = (QuestData.Difficulty)i;
            questSelectButtons[index].onClick.AddListener(() => { ToBattle(difficulty); });
            index++;
        }
        int a = 0;
        for(int i = 1; i <= (int)PlayerData.PlayerRank.Special; i <<= 1)
        {
            PlayerData.PlayerRank playerRank = (PlayerData.PlayerRank)i;
            playerRankButtons[a].onClick.AddListener(() => { SetPlayerData(playerRank); });
            a++;

        }
        toTitleButton.onClick.AddListener(() => { StartCoroutine(SceneFader.Instance.SceneChange(SceneFader.SceneTitle.Title,0)); });
        //rankingButton.onClick.AddListener(ShowScoreRanking);
        selectPlayerRankButton.onClick.AddListener(() => { PopupWindowController.Popup(playerRankBoard,true); });
        windowCloseButton[0].onClick.AddListener(() => { PopupWindowController.Popup(rankingBoard,false); });
        windowCloseButton[1].onClick.AddListener(() => { PopupWindowController.Popup(playerRankBoard,false); });
        rankingButton.onClick.AddListener(() => { ShowScoreRanking(ScoreRanking.Instance.ScoreRankingList); });
    }

    /// <summary>プレーヤーランクを設定する</summary>
    /// <param name="rank"></param>
    void SetPlayerData(PlayerData.PlayerRank rank)
    {
        progress.CurrentPlayerRank = rank;
        foreach(var item in currentSelectIcons)
        {
            item.GetComponent<CurrentPlayerRank>().SetCurrentRankText();
        }

        switch(rank)
        {
            case PlayerData.PlayerRank.Low:
                soundManager.PlayWithFade(SoundAsset.SE.Player_LowVoice);
                break;
            case PlayerData.PlayerRank.Standard:
                soundManager.PlayWithFade(SoundAsset.SE.Player_StandardVoice);
                break;
            case PlayerData.PlayerRank.High:
                soundManager.PlayWithFade(SoundAsset.SE.Player_HighVoice);
                break;
            case PlayerData.PlayerRank.Special:
                soundManager.PlayWithFade(SoundAsset.SE.Player_SpecialVoice);
                break;
            default:
                break;
        }
        Debug.Log(progress.CurrentPlayerRank);
        //      PopupWindowController.Popup(playerRankBoard,false);
    }

    private void ToBattle(QuestData.Difficulty difficulty)
    {
        progress.CurrentQuest = difficulty;
        StartCoroutine(SceneFader.Instance.SceneChange(SceneFader.SceneTitle.Battle,0));
    }

    /// <summary>スコアランキングを表示</summary>
    void ShowScoreRanking(List<QuestResultData> scoreRankings)
    {
        PopupWindowController.Popup(rankingBoard,true);

        for(int i = 0; i < scoreRankings.Count; i++)
        {
            if(scoreRankings[i]._totalScore == 0)
            {
                scoreTexts[i].text = "-";
                playerRankTexts[i].text = "-";
                playerHpTexts[i].text = "-";
                enemykillTexts[i].text = "-";
                clearTimeTexts[i].text = "-";
            }
            else
            {
                scoreTexts[i].text = scoreRankings[i]._totalScore.ToString();
                playerHpTexts[i].text = scoreRankings[i]._playerHp.ToString();
                enemykillTexts[i].text = scoreRankings[i]._totalKillNumber.ToString();
                clearTimeTexts[i].text = scoreRankings[i]._clearTime.ToString("f0");

                switch(scoreRankings[i]._playerRank)
                {
                    case PlayerData.PlayerRank.Low:
                        playerRankTexts[i].text = "低級";
                        break;
                    case PlayerData.PlayerRank.Standard:
                        playerRankTexts[i].text = "標準";
                        break;
                    case PlayerData.PlayerRank.High:
                        playerRankTexts[i].text = "高級";
                        break;
                    case PlayerData.PlayerRank.Special:
                        playerRankTexts[i].text = "特級";
                        break;
                    default:
                        break;
                }
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class CancelSEPlayer : MonoBehaviour
{
    SoundManager soundManager = null;
    [SerializeField] Button button = null;
    void Awake()
    {
        soundManager = SoundManager.Instance;
        if(button == null)
        {
            button = GetComponent<Button>();
        }
        button.onClick.AddListener(() =>
        {
            soundManager.PlayWithFade(SoundAsset.SE.Cancel);
        });
    }
}
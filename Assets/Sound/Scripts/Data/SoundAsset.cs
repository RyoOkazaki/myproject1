﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "SoundData",menuName = "CreSounds")]
public class SoundAsset : ScriptableObject
{
    public enum BGM
    {
        Title,
        Menu,
        Result,
        Battle_Level1,
        Battle_Level2,
        Battle_Level3,
        Battle_Level4,
    }

    public enum SE
    {
        /// <summary>肯定</summary>
        Submit,
        /// <summary>否定</summary>
        Cancel,
        Clear,
        GameOver,
        GradeUp,
        Player_LowVoice,
        Player_StandardVoice,
        Player_HighVoice,
        Player_SpecialVoice,
    }

    public enum BattleSE
    {
        LaserHit,
        EnemyDestroy,
        EnemySpawn,
    }

    public AudioSource[] audioSources;
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "BattleSE",menuName = "Sounds/BattleSE")]
public class BattleSEAsset : ScriptableObject
{

    [Serializable]
    public class BattleSE
    {
        public SoundAsset.BattleSE id;
        public AudioClip clip;
    }
    [SerializeField] List<BattleSE> battleSe = new List<BattleSE>();

    public AudioClip GetClip(SoundAsset.BattleSE BattleSETag)
    {
        foreach(BattleSE item in battleSe)
        {
            if(item.id != BattleSETag)
                continue;
            return item.clip;
        }
        return null;
    }
}
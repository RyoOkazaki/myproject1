﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;

using SceneTitle = SceneFader.SceneTitle;
[Serializable, CreateAssetMenu(fileName = "SnapData",menuName = "Sound/Snap")]
public class SnapAsset : ScriptableObject
{
    [Serializable]
    public class SceneSnap
    {
        public SceneTitle title;
        public AudioMixerSnapshot snap;
    }

    public float interval = 1f;
    [SerializeField] List<SceneSnap> snaps = new List<SceneSnap>();

    public Dictionary<SceneTitle,AudioMixerSnapshot> SceneSnaps
    {
        get
        {
            Dictionary<SceneTitle,AudioMixerSnapshot> tmp = new Dictionary<SceneTitle,AudioMixerSnapshot>();
            foreach(SceneSnap item in snaps)
            {
                tmp.Add(item.title,item.snap);
            }
            return tmp;
        }
    }

}
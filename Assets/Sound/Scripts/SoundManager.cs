﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class SoundManager : MonoSingleton<SoundManager>
{
    public enum SoundType
    {
        BGM,
        SE,
        BattleSE,
        Master,
    }

    List<AudioSource> BgmSources = new List<AudioSource>();
    List<AudioSource> SESources = new List<AudioSource>();
    List<AudioSource> BattleSESources = new List<AudioSource>();

    Dictionary<SoundType,List<AudioSource>> AudioSources
    {
        get
        {
            Dictionary<SoundType,List<AudioSource>> tmp = new Dictionary<SoundType,List<AudioSource>>();
            tmp.Add(SoundType.BGM,BgmSources);
            tmp.Add(SoundType.SE,SESources);
            tmp.Add(SoundType.BattleSE,BattleSESources);
            return tmp;
        }
    }

    SoundAsset GetBGMAsset { get { return soundAssets[(int)SoundType.BGM]; } }
    SoundAsset GetSEAsset { get { return soundAssets[(int)SoundType.SE]; } }
    SoundAsset GetBattleSEAsset { get { return soundAssets[(int)SoundType.BattleSE]; } }


    public AudioMixer audioMixer;
    [SerializeField] SoundAsset[] soundAssets;
    [SerializeField] BGMAsset bgmAsset;
    [SerializeField] SEAsset seAsset;
    [SerializeField] BattleSEAsset battleSeAsset;
    [SerializeField] SnapAsset snapAsset;

    public override void OnInitialize()
    {
        base.OnInitialize();
        DontDestroyOnLoad(Instance);
        Init();
    }

    private void OnApplicationQuit()
    {
        StopWithFade();
    }

    SoundAsset.BGM GetBGM(SceneFader.SceneTitle title)
    {
        SoundAsset.BGM bgm = SoundAsset.BGM.Title;
        switch(title)
        {
            case SceneFader.SceneTitle.Title:
                bgm = SoundAsset.BGM.Title;
                break;
            case SceneFader.SceneTitle.Menu:
                bgm = SoundAsset.BGM.Menu;
                break;
            case SceneFader.SceneTitle.Battle:
                switch(Progress.Instance.CurrentQuest)
                {
                    case QuestData.Difficulty.Level1:
                        bgm = SoundAsset.BGM.Battle_Level1;
                        break;
                    case QuestData.Difficulty.Level2:
                        bgm = SoundAsset.BGM.Battle_Level2;
                        break;
                    case QuestData.Difficulty.Level3:
                        bgm = SoundAsset.BGM.Battle_Level3;
                        break;
                    case QuestData.Difficulty.Level4:
                        bgm = SoundAsset.BGM.Battle_Level4;
                        break;
                    case QuestData.Difficulty.All:
                        break;
                    default:
                        break;
                }
                break;
            case SceneFader.SceneTitle.Result:
                bgm = SoundAsset.BGM.Result;
                break;
            default:
                break;
        }
        return bgm;
    }

    void BGMCheck(Scene scene,LoadSceneMode mode)
    {
        if(mode == LoadSceneMode.Additive)
        {
            return;
        }
        if(Enum.TryParse(scene.name,out SceneFader.SceneTitle sceneTitle))
        {
            AudioMixerSnapshot snap = snapAsset.SceneSnaps[sceneTitle];
            snap.TransitionTo(snapAsset.interval);
            //LoadVolume();
            PlayWithFade(GetBGM(sceneTitle));
        }
    }

    public void Init()
    {
        SceneManager.sceneLoaded += BGMCheck;

        if(BgmSources.Count * SESources.Count * BattleSESources.Count == 0)
        {
            foreach(Transform item in transform)
            {
                Destroy(item.gameObject);
            }
            BgmSources.Clear();
            SESources.Clear();
            BattleSESources.Clear();
            int nametag = 0;
            foreach(AudioSource source in GetBGMAsset.audioSources)
            {
                nametag++;
                var tmp = Numbering(Instantiate(source.gameObject,transform),nametag).GetComponent<AudioSource>();
                tmp.outputAudioMixerGroup = source.outputAudioMixerGroup;

                BgmSources.Add(tmp);
            }
            foreach(AudioSource source in GetSEAsset.audioSources)
            {
                nametag++;
                var tmp = Numbering(Instantiate(source.gameObject,transform),nametag).GetComponent<AudioSource>();
                tmp.outputAudioMixerGroup = source.outputAudioMixerGroup;

                SESources.Add(tmp);
            }
            foreach(AudioSource source in GetBattleSEAsset.audioSources)
            {
                nametag++;
                var tmp = Numbering(Instantiate(source.gameObject,transform),nametag).GetComponent<AudioSource>();
                tmp.outputAudioMixerGroup = source.outputAudioMixerGroup;

                BattleSESources.Add(tmp);
            }
        }

        BGMCheck(SceneManager.GetActiveScene(),LoadSceneMode.Single);

    }

    GameObject Numbering(GameObject game,int i)
    {
        game.name += i.ToString();
        return game;
    }

    float GetVolume(SoundType soundType)
    {
        float volume = 0;
        switch(soundType)
        {
            case SoundType.BGM:
                volume = PlayerPrefs.GetFloat(bgmVolumeKey);
                break;
            case SoundType.SE:
                volume = PlayerPrefs.GetFloat(seVolumeKey);
                break;
            case SoundType.BattleSE:
                volume = PlayerPrefs.GetFloat(battleSeVolumeKey);
                break;
            case SoundType.Master:
                break;
            default:
                break;
        }
        return volume;
    }

    public Volume CurrentVol
    {
        get
        {
            Volume volume = JsonUtility.FromJson<Volume>(PlayerPrefs.GetString(volumeKey));

            if(volume == null)
            {
                return new Volume(0,0,0);
            }
            return volume;
        }
    }

    public readonly string volumeKey = "VolumeKey";
    public readonly string bgmVolumeKey = "BGMVolume";
    public readonly string seVolumeKey = "SEVolume";
    public readonly string battleSeVolumeKey = "BattleSEVolume";

    public void SaveVolume()
    {
        string volumeData = JsonUtility.ToJson(new Volume(GetVolume(SoundType.BGM),GetVolume(SoundType.SE),GetVolume(SoundType.BattleSE)));
        PlayerPrefs.SetString(volumeKey,volumeData);
    }

    public void LoadVolume()
    {
        Volume volume = JsonUtility.FromJson<Volume>(PlayerPrefs.GetString(volumeKey));

        if(volume == null)
        {
            volume = new Volume();
        }

        audioMixer.SetFloat(bgmVolumeKey,BGMVolume);
        audioMixer.SetFloat(seVolumeKey,SEVolume);
        audioMixer.SetFloat(battleSeVolumeKey,BattleSEVolume);
    }

    public float BGMVolume
    {
        get
        {
            float v = PlayerPrefs.GetFloat(bgmVolumeKey);
            return v;
        }
        set
        {
            audioMixer.SetFloat(bgmVolumeKey,Mathf.Lerp(-80,0,value));
        }
    }

    public float SEVolume
    {
        get
        {
            float v = PlayerPrefs.GetFloat(seVolumeKey);
            return v;
        }
        set
        {
            audioMixer.SetFloat(seVolumeKey,Mathf.Lerp(-80,0,value));
        }
    }

    public float BattleSEVolume
    {
        get
        {
            float v = PlayerPrefs.GetFloat(battleSeVolumeKey);
            return v;
        }
        set
        {
            audioMixer.SetFloat(battleSeVolumeKey,Mathf.Lerp(-80,0,value));
        }
    }

    public void SetBGMVolume(float volume)
    {
        audioMixer.SetFloat(bgmVolumeKey,volume);
    }
    public void SetSEVolume(float volume)
    {
        audioMixer.SetFloat(seVolumeKey,volume);
    }
    public void SetBattleSEVolume(float volume)
    {
        audioMixer.SetFloat(battleSeVolumeKey,volume);
    }

    [Serializable]
    public class Volume
    {
        public float[] volumes = new float[3];
        public Volume()
        {
            volumes = new float[3] { 0,0,0 };
        }
        public Volume(float bgm,float se,float battleSe)
        {
            volumes[0] = bgm;
            volumes[1] = se;
            volumes[2] = battleSe;
        }
    }


    void PlaySupport(List<AudioSource> list,AudioClip clip,float fade = 0.1f)
    {
        AudioSource emptySouce = list.FirstOrDefault(x => x.isPlaying == false);
        AudioSource playingSouce = list.FirstOrDefault(x => x.isPlaying == true);
        AudioSource def = default;

        if(def != playingSouce)
        {
            if(playingSouce.clip == clip)
            {
                return;
            }
            StartCoroutine(playingSouce.StopWithFadeOut(fade));
            if(def == emptySouce)
            {
                emptySouce = list.FirstOrDefault(x => x.GetHashCode() != playingSouce.GetHashCode());
            }
        }
        StartCoroutine(emptySouce.PlayWithFadeIn(clip,fade));
    }

    /// <summary>音源再生</summary>
    /// <param name="tag">再生する音の種類</param>
    /// <param name="clip">音源</param>
    /// <param name="fade">二種類以上ある場合の音のフェード時間</param>
    public void PlayWithFade(SoundType tag,AudioClip clip,float fade = 0.1f)
    {
        List<AudioSource> tmpList = new List<AudioSource>();
        switch(tag)
        {
            case SoundType.BGM:
                tmpList = BgmSources;
                break;
            case SoundType.SE:
                tmpList = SESources;
                break;
            case SoundType.BattleSE:
                tmpList = BattleSESources;
                break;
            default:
                break;
        }
        PlaySupport(tmpList,clip,fade);
    }
    /// <summary>音源再生</summary>
    /// <param name="bgm">流すBGMの種類</param>
    /// <param name="fade">二種類以上ある場合の音のフェード時間</param>
    public void PlayWithFade(SoundAsset.BGM bgm,float fade = 0.1f)
    {
        AudioClip clip = bgmAsset.GetClip(bgm);

        PlaySupport(BgmSources,clip,fade);

    }
    /// <summary>音源再生</summary>
    /// <param name="tag">流すSEの種類</param>
    /// <param name="fade">二種類以上ある場合の音のフェード時間</param>
    public void PlayWithFade(SoundAsset.SE tag,float fade = 0.1f)
    {
        AudioClip clip = seAsset.GetClip(tag);

        PlaySupport(SESources,clip,fade);
    }
    /// <summary>
    /// サテライトのスキルSE再生
    /// </summary>
    /// <param name="skillSEtag"></param>
    /// <param name="fade"></param>
    public void PlayWithFade(SoundAsset.BattleSE skillSEtag,float fade = 0.1f)
    {
        AudioClip clip = battleSeAsset.GetClip(skillSEtag);

        PlaySupport(BattleSESources,clip,fade);
    }

    /// <summary>
    /// 全AudioSource再生停止
    /// </summary>
    void StopWithFade()
    {
        BgmSources.ForEach(x => StartCoroutine(x.StopWithFadeOut()));
        SESources.ForEach(x => StartCoroutine(x.StopWithFadeOut()));
        BattleSESources.ForEach(x => StartCoroutine(x.StopWithFadeOut()));
    }

    /// <summary>
    /// AudioSourceごとに再生停止
    /// </summary>
    /// <param name="tag">停止したいAudioSourceのTag</param>
    public void StopWithFade(SoundType tag)
    {
        switch(tag)
        {
            case SoundType.BGM:
                BgmSources.ForEach(x => StartCoroutine(x.StopWithFadeOut()));
                break;
            case SoundType.SE:
                SESources.ForEach(x => StartCoroutine(x.StopWithFadeOut()));
                break;
            case SoundType.BattleSE:
                BattleSESources.ForEach(x => StartCoroutine(x.StopWithFadeOut()));
                break;
            default:
                break;
        }
    }
}
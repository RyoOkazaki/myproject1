﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ComboUIController : MonoBehaviour
{
    void OnEnable()
    {
        transform.DOLocalMoveY(transform.position.y + 2f,1f).OnComplete(() =>
         {
             gameObject.SetActive(false);
         });
    }
}

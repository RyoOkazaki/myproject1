﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class BattleSceneManager : MonoSingleton<BattleSceneManager>
{
    [SerializeField] Image screenImage = null;
    [SerializeField] GameObject clearText = null;
    [SerializeField] GameObject gameoverText = null;
    [SerializeField] GameObject player = null;
    [SerializeField] GameObject pauseMenu = null;
    [SerializeField] Image backGround = null;
    [SerializeField] Button pauseMenuButton = null;
    [SerializeField] Button closeButton = null;
    [SerializeField] Button retryButton = null;
    [SerializeField] Button backMenuButton = null;

    SceneFader sceneFader;
    SoundManager soundManager;
    PlayerController playerController;
    Progress progress;
    QuestManager questManager;
    QuestData questData;
    /// <summary>殺した敵の総数</summary>
    public int totalKillEnemy = 0;
    private void Awake()
    {
        progress = Progress.Instance;
        soundManager = SoundManager.Instance;
        sceneFader = SceneFader.Instance;
        questManager = QuestManager.Instance;
        playerController = PlayerController.Instance;
    }

    private void Start()
    {
        questData = questManager.GetQuestData(questManager.questDataList);
        backGround.sprite = questData.backGround;
        gameoverText.SetActive(false);
        clearText.SetActive(false);
        pauseMenu.SetActive(false);
        pauseMenuButton.onClick.AddListener(() => OpenPauseMenu());
        closeButton.onClick.AddListener(() => ClosePauseMenu());
        retryButton.onClick.AddListener(() => StartCoroutine(sceneFader.SceneChange(SceneFader.SceneTitle.Battle,0)));
        backMenuButton.onClick.AddListener(() => StartCoroutine(sceneFader.SceneChange(SceneFader.SceneTitle.Menu,0)));
    }

#if UNITY_EDITOR
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.C))//強制クリア
        {
            DeleteEnemies();
        }
    }
#endif

    /// <summary>ポーズメニューを開く</summary>
    void OpenPauseMenu()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
    }

    /// <summary>ポーズメニューを閉じる</summary>
    void ClosePauseMenu()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
    }

    public void FlashScreen()
    {
        StartCoroutine(ScreenFlash.DamagedFlashScreen(screenImage));
    }

    /// <summary>ゲームオーバー</summary>
    public void GameOver()
    {
        soundManager.StopWithFade(SoundManager.SoundType.BGM);
        player.SetActive(false);
        gameoverText.SetActive(true);
        StartCoroutine(sceneFader.SceneChange(SceneFader.SceneTitle.Menu,3f));
    }

    /// <summary>クリア</summary>
    public void DeleteEnemies()
    {
        GameObject[] enemys = GameObject.FindGameObjectsWithTag("Enemy");
        foreach(var enemy in enemys)
        {
            Destroy(enemy);
        }

        soundManager.StopWithFade(SoundManager.SoundType.BGM);
        soundManager.PlayWithFade(SoundAsset.SE.Clear);
        //クリアしたクエストのフラグを立てる
        if(!progress.ClearedQuest.HasFlag(questData.difficulty))
        {
            progress.FlagClearedQuest(questData.difficulty);
        }

        clearText.SetActive(true);
        float elapsedTime = Time.time;
        questManager.SetResultData(Progress.Instance.CurrentPlayerRank,playerController.hp,
            ScoreManager.Instance.totalScore,totalKillEnemy,elapsedTime);

        StartCoroutine(sceneFader.SceneChange(SceneFader.SceneTitle.Result,3f));
    }

    /// <summary>敵の巣を全て破壊したらクリアとする</summary>
    /// <param name="currentSpawnPoint"></param>
    public void CheckClear(int currentSpawnPoint)
    {
        if(currentSpawnPoint <= 0)
        {
            DeleteEnemies();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
public class SpawnManager : MonoSingleton<SpawnManager>
{
    [SerializeField] GameObject player = null;
    QuestManager questManager;
    SoundManager soundManager;
    BattleSceneManager sceneManager;
    /// <summary>生成する敵の総数</summary>
    public int totalGenerateEnemy = 0;
    /// <summary>生成頻度</summary>
    public List<float> spawnInterval = new List<float>();
    GameObject[] spawnPoints;

    /// <summary>現在の生成ポイントの数</summary>
    public int currentSpawnCount = 0;

    [SerializeField] GameObject[] enemyGenerators = null;
    private void Awake()
    {
        soundManager = SoundManager.Instance;
        sceneManager = BattleSceneManager.Instance;
        questManager = QuestManager.Instance;
    }

    void Start()
    {
        for(int i = 0; i < enemyGenerators.Length; i++)
        {
            enemyGenerators[i].SetActive(false);
        }

        var questData = questManager.GetQuestData(questManager.questDataList);
        switch(questData.difficulty)
        {
            case QuestData.Difficulty.Level1:
                enemyGenerators[0].SetActive(true);
                break;
            case QuestData.Difficulty.Level2:
                enemyGenerators[1].SetActive(true);
                break;
            case QuestData.Difficulty.Level3:
                enemyGenerators[2].SetActive(true);
                break;
            case QuestData.Difficulty.Level4:
                enemyGenerators[3].SetActive(true);
                break;
            default:
                break;
        }
        spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
        currentSpawnCount = spawnPoints.Length;

        for(int i = 0; i < questData.generateEnemies.Count; i++)
        {
            StartCoroutine(GenerateEnemy(questData,i));
        }
    }

    /// <summary>敵を生成</summary>
    /// <param name="questData"></param>
    /// <param name="generateObject"></param>
    /// <returns></returns>
    IEnumerator GenerateEnemy(QuestData questData,int enemyIndex)
    {
        spawnInterval.Add(questData.generateInterval[enemyIndex]);

        //1体目生成
        float firstSpawnTime = UnityEngine.Random.Range(questData.firstGenerateTime[enemyIndex] - 2f,questData.firstGenerateTime[enemyIndex] + 2f);
        yield return new WaitForSeconds(firstSpawnTime);

        if(currentSpawnCount <= 0)
        {
            yield break;
        }
        GameObject enemy = null;
        enemy = Instantiate(questData.generateEnemies[enemyIndex].enemy);
       // soundManager.PlayWithFade(SoundAsset.BattleSE.EnemySpawn);
        spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
        int index = UnityEngine.Random.Range(0,spawnPoints.Length);
        enemy.transform.position = new Vector3(spawnPoints[index].transform.position.x,spawnPoints[index].transform.position.y,player.transform.position.z);


        //2体目以降は一定の頻度で生成
        while(currentSpawnCount > 0)
        {
            yield return new WaitForSeconds(spawnInterval[enemyIndex]);
            if(currentSpawnCount <= 0)
            {
                yield break;
            }
            enemy = Instantiate(questData.generateEnemies[enemyIndex].enemy);
          //  soundManager.PlayWithFade(SoundAsset.BattleSE.EnemySpawn);
            spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
            index = UnityEngine.Random.Range(0,spawnPoints.Length);
            enemy.transform.position = new Vector3(spawnPoints[index].transform.position.x,spawnPoints[index].transform.position.y,player.transform.position.z);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>クエストデータ</summary>
[SerializeField]
[CreateAssetMenu(fileName = "QuestData")]
public class QuestData : ScriptableObject
{
    /// <summary>難易度</summary>
    [Flags]
    public enum Difficulty
    {
        Level1 = 1,
        Level2 = 2,
        Level3 = 4,
        Level4 = 8,
        All = 15,
    }

    /// <summary>生成する敵オブジェクト</summary>
    public enum GenerateObject
    {
        Wiener,
        Steak,
        Chicken,
        RawMeat,
        GoldenBird,
    }

    [Header("クエストの難易度")]
    public Difficulty difficulty = Difficulty.Level1;

    [Header("生成するオブジェクト")]
    public List<GenerateEnemy> generateEnemies = new List<GenerateEnemy>();

    [Header("初期生成時間")]
    public List<float> firstGenerateTime = new List<float>();

    [Header("生成頻度")]
    public List<float> generateInterval = new List<float>();

    [Header("生成速度上昇倍率")]
    public float generateSpeedMagnification;

    [Header("背景")]
    public Sprite backGround;

    [Serializable]
    public class GenerateEnemy
    {
        public GenerateObject generateObject;
        public GameObject enemy;
    }
}

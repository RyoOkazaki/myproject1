﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public static class PopupWindowController
{
    /// <summary>ウィンドウをポップアップする</summary>
    /// <param name="window">ポップアップさせたいオブジェクト</param>
    /// <param name="isOpen">trueなら開く、falseなら閉じる</param>
    public static void Popup(GameObject window,bool isOpen)
    {
        if(isOpen)
        {
            window.SetActive(true);
            window.GetComponent<RectTransform>().localScale = new Vector3(0,0,1);
            window.GetComponent<RectTransform>().DOScale(new Vector3(1,1,1),1);
        }
        else
        {
            window.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
            window.GetComponent<RectTransform>().DOScale(new Vector3(0,0,1),1).OnComplete(() =>
            {
                window.SetActive(false);
            });
        }
    }
}

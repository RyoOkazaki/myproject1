﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class QuestManager : SingletonBase<QuestManager>
{
    public List<QuestData> questDataList = new List<QuestData>();

    protected override void OnInitialize()
    {
        questDataList.Clear();
        QuestData[] tmpData = Resources.LoadAll<QuestData>("QuestData");
        questDataList = tmpData.ToList();
    }

    //Progress progress = null;
    //Progress GetProgress
    //{
    //    get
    //    {
    //        if(progress == null)
    //        {
    //            progress = Progress.Instance;
    //        }
    //        return progress;
    //    }
    //}

    /// <summary>現在のクエスト情報を取得</summary>
    /// <param name="questDataList"></param>
    /// <returns></returns>
    public QuestData GetQuestData(List<QuestData> questDataList)
    {
        var questData = questDataList.Find(quest => quest.difficulty == Progress.Instance.CurrentQuest);
        if(questData == null)
        {
            Progress.Instance.CurrentQuest = QuestData.Difficulty.Level1;
            questData = questDataList.Find(quest => quest.difficulty == Progress.Instance.CurrentQuest);
        }
        return questData;
    }

    /// <summary>クエスト結果を入力する</summary>
    /// <param name="difficulty"></param>
    /// <param name="grade"></param>
    /// <param name="score"></param>
    /// <param name="kill"></param>
    public void SetResultData(PlayerData.PlayerRank playerRank,int hp,int score,int kill,float clearTime)
    {
        GetResultData = new QuestResultData(playerRank,hp,score,kill,clearTime);
        ScoreRanking.Instance.SortArray(GetResultData);
    }

    /// <summary>クエスト結果を取得する</summary>
    public QuestResultData GetResultData { get; private set; }

}

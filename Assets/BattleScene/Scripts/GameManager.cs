﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Linq;
using System;

public class GameManager : MonoSingleton<GameManager>
{
    private void Awake()
    {
        Time.timeScale = 1;
        SceneManager.sceneLoaded += (scene,mode) =>
        {
            if(mode != LoadSceneMode.Additive)
            {
                SceneFader.Instance.FadeIn();
            }
        };
        DontDestroyOnLoad(gameObject);
    }
}
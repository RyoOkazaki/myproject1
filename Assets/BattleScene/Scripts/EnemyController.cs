﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour
{
    /// <summary>敵の個体識別</summary>
    public enum EnemyType
    {
        Winner,
        Steak,
        Chicken,
        RawMeat,
        GoldenBird,
    }
    SoundManager soundManager;
    LineRenderer lineRenderer;
    PlayerController playerController;
    public EnemyType enemyType;
    [SerializeField] public int enemyMaxHp = 3;
    public int hp;
    [SerializeField] public float moveTime = 1;
    GameObject player = null;
    [SerializeField] float eatingSpeed = 3f;
    Sequence sequence;
    BattleSceneManager battleSceneManager;
    private string playerTag = "Player";

    private void Awake()
    {
        soundManager = SoundManager.Instance;
        sequence = DOTween.Sequence();
        playerController = PlayerController.Instance;
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = false;
        battleSceneManager = BattleSceneManager.Instance;
    }

    private void Start()
    {
        player = playerController.gameObject;

        hp = enemyMaxHp;

        if(player != null)
        {
            sequence.Append(transform.DOLocalMove(player.transform.position,moveTime));
        }
    }


    /// <summary>プレーヤーを捕食する！</summary>
    /// <param name="attackInterval"></param>
    /// <returns></returns>
    IEnumerator AttackPlayer(float attackInterval)
    {
        lineRenderer.enabled = true;
        lineRenderer.SetPosition(0,transform.position);
        lineRenderer.SetPosition(1,player.transform.position);

        while(hp > 0)
        {
            yield return new WaitForSeconds(attackInterval);
            ShotLaser(lineRenderer.GetPosition(0),lineRenderer.GetPosition(1));
        }
    }

    /// <summary>レーザーを発射する</summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    void ShotLaser(Vector3 from,Vector3 to)
    {
        Ray ray = new Ray(from,to - from);
        RaycastHit hit;

        if(Physics.Raycast(ray,out hit))
        {
            if(hit.collider.tag == playerTag)
            {
                playerController.hp--;
                soundManager.PlayWithFade(SoundManager.SoundType.BattleSE,playerController.damageVoice);
                battleSceneManager.FlashScreen();
                AttackComboManager.Instance.ResetCombo();

                if(playerController.hp <= 0)
                {
                    playerController.hp = 0;
                    soundManager.PlayWithFade(SoundManager.SoundType.BattleSE,playerController.deadVoice);
                    battleSceneManager.GameOver();
                }
                playerController.UpdateStatusUI();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == playerTag)
        {
            sequence.Pause();
            StartCoroutine(AttackPlayer(eatingSpeed));
        }
    }
}

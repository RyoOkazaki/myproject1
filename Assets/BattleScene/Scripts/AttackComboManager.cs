﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AttackComboManager : MonoSingleton<AttackComboManager>
{
    [SerializeField] GameObject comboUI = null;
    /// <summary>経験値ゲージ</summary>
    [SerializeField] Slider expGauge = null;
    /// <summary>現在のコンボ数</summary>
    public int currentCombo = 0;
    public float expGaugeMaxvalue = 100;
    PlayerController playerController;
    SpawnManager spawnManager;
    QuestManager questManager;
    [SerializeField] GameObject gradeupText = null;
    QuestData questData;
    [SerializeField] BattleTutorialSetter battleTutorialSetter;
    private void Awake()
    {
        questManager = QuestManager.Instance;
        spawnManager = SpawnManager.Instance;
        playerController = PlayerController.Instance;
    }

    private void Start()
    {
        questData = questManager.GetQuestData(questManager.questDataList);
        gradeupText.SetActive(false);
        expGauge.maxValue = playerController.playerData.firstNeedExp;
        expGauge.value = 0;
    }

    /// <summary>コンボUIを表示する</summary>
    /// <param name="addExp"></param>
    /// <param name="position"></param>
    public void PopupComboUI(int addExp,Vector3 position)
    {
        comboUI.transform.position = position;
        comboUI.SetActive(true);
        expGauge.value += addExp;

        if(expGauge.value >= expGauge.maxValue)//経験値ゲージが最大になったら
        {
            GradeUpPlayer();
            if(!Progress.Instance.UnlockedTutorial.HasFlag(Progress.Tutorial.Battle))
            {
                battleTutorialSetter.ResumeTutorial();
            }
        }

        currentCombo++;
        comboUI.GetComponent<TextMesh>().text = "COMBO " + currentCombo.ToString();
    }

    public void ResetCombo()
    {
        currentCombo = 0;
    }

    /// <summary>プレーヤーを強化する</summary>
   private void GradeUpPlayer()
    {
        SoundManager.Instance.PlayWithFade(SoundAsset.SE.GradeUp);
        playerController.SpeedUpLaserRotate(1.1f);
        playerController.ExtendLaser();

        gradeupText.SetActive(true);

        for(int i = 0; i < spawnManager.spawnInterval.Count; i++)
        {
            spawnManager.spawnInterval[i] *= questData.generateSpeedMagnification;//敵の生成速度を上げる
            if(spawnManager.spawnInterval[i] < 1f)
            {
                spawnManager.spawnInterval[i] = 1f;
            }
            Debug.Log($"生成頻度：{spawnManager.spawnInterval[i]}");
        }
        expGauge.maxValue += playerController.playerData.addAmountExp;
        expGauge.value = 0;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoSingleton<ScoreManager>
{
    [SerializeField] Text scoreText = null;
    public int totalScore = 0;

    private void Start()
    {
        scoreText.text = totalScore.ToString();
    }

    public void AddScore(EnemyController.EnemyType enemyType)
    {
        switch(enemyType)
        {
            case EnemyController.EnemyType.Winner:
                totalScore += 10;
                break;
            case EnemyController.EnemyType.Steak:
                totalScore += 20;
                break;
            case EnemyController.EnemyType.Chicken:
                totalScore += 30;
                break;
            default:
                break;
        }
        totalScore += AttackComboManager.Instance.currentCombo * 2;
        scoreText.text = totalScore.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseSetActiveObject : MonoBehaviour
{
    protected virtual void SetActive(QuestData.Difficulty difficulty)
    {
        if(Progress.Instance.ClearedQuest.HasFlag(difficulty))
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}

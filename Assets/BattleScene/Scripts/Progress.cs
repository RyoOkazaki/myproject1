﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Progress : SavableSingletonBase<Progress>
{
    [SerializeField] QuestData.Difficulty currentQuest = 0;
    [SerializeField] QuestData.Difficulty clearedQuest = 0;
    [SerializeField] PlayerData.PlayerRank currentPlayerRank = PlayerData.PlayerRank.Low;
    [SerializeField] Tutorial unlockedTutorial = 0;

    [Flags]
    public enum Tutorial
    {
        Menu = 1,
        Battle = 2,
    }

    /// <summary>現在選択中のクエスト</summary>
    public QuestData.Difficulty CurrentQuest
    {
        get
        {
            return currentQuest;
        }
        set
        {
            currentQuest = value;
            Save();
        }
    }

    /// <summary>クリア済みのクエスト</summary>
    public QuestData.Difficulty ClearedQuest
    {
        get
        {
            return clearedQuest;
        }
        set
        {
            clearedQuest = value;
            Save();
        }
    }

    /// <summary>選択中のプレーヤーランク</summary>
    public PlayerData.PlayerRank CurrentPlayerRank
    {
        get
        {
            if(currentPlayerRank == 0)
            {
                currentPlayerRank = PlayerData.PlayerRank.Low;
            }
            return currentPlayerRank;
        }
        set
        {
            currentPlayerRank = value;
            Save();
        }
    }

    public Tutorial UnlockedTutorial
    {
        get { return unlockedTutorial; }
        set
        {
            unlockedTutorial = value;
            Save();
        }
    }

    /// <summary>終了したチュートリアルのフラグを立てる</summary>
    /// <param name="flag"></param>
    public void FlagTutorial(Tutorial flag)
    {
        UnlockedTutorial |= flag;
    }

    /// <summary>クリアしたクエストのフラグを立てる</summary>
    /// <param name="difficulty"></param>
    public void FlagClearedQuest(QuestData.Difficulty difficulty)
    {
        ClearedQuest |= difficulty;
    }
}

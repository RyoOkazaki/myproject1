﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActiveObject : BaseSetActiveObject
{
    [SerializeField] QuestData.Difficulty difficulty;
    void Start()
    {
        SetActive(difficulty);
    }

    protected override void SetActive(QuestData.Difficulty difficulty)
    {
        base.SetActive(difficulty);
    }
}

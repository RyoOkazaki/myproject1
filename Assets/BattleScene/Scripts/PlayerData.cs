﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "PlayerData")]
public class PlayerData : ScriptableObject
{
    /// <summary>プレーヤーの価値</summary>
    [Flags]
    public enum PlayerRank
    {
        /// <summary>低級</summary>
        Low = 1,
        /// <summary>標準</summary>
        Standard = 2,
        /// <summary>高級</summary>
        High = 4,
        /// <summary>特級</summary>
        Special = 8,
    }

    /// <summary>ランク</summary>
    public PlayerRank playerRank = PlayerRank.Low;

    /// <summary>体力</summary>
    public int _hp = 1;

    /// <summary>レーザーの数</summary>
    public uint _laserNumber = 1;

    /// <summary>プレーヤーの見た目</summary>
    public Material materials;

    /// <summary>初回の強化に必要な経験値</summary>
    public float firstNeedExp;

    /// <summary>経験値の上限値増加量</summary>
    public float addAmountExp;

    /// <summary>攻撃を受けたときのボイス</summary>
    public AudioClip damageVoice;

    /// <summary>戦闘不能時のボイス</summary>
    public AudioClip deadVoice;
}



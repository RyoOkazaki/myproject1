﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayController : MonoBehaviour
{
    LineRenderer lineRenderer;
    [SerializeField] float distance = 10f;
    [SerializeField] GameObject endPosObj = null;
    BoxCollider2D rayCollider;
    List<Vector2> points = new List<Vector2>();
    [SerializeField] GameObject thunderEffect = null;

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.SetPosition(0,transform.position);
        lineRenderer.SetPosition(1,endPosObj.transform.position);


        rayCollider = gameObject.AddComponent<BoxCollider2D>();
        rayCollider.isTrigger = true;
        rayCollider.offset = new Vector2(0,5);
       // rayCollider.points = lineRenderer.
       //// rayCollider.transform.position = (endPosObj.transform.position - transform.position) / 2;
       // rayCollider.points = pointList.ToArray();
    }

    void Update()
    {
        lineRenderer.SetPosition(1,endPosObj.transform.position);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            GameObject effect = Instantiate(thunderEffect);
            effect.transform.position = collision.transform.position;
            Debug.Log("hit");
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoSingleton<PlayerController>
{
    public int hp;
    private LaserController[] laserControllers;
    private uint myLaserNumber = 0;
    public List<PlayerData> playerDataList;
    [SerializeField] Text hpText = null;
    [SerializeField] Slider hpGauge = null;
    public PlayerData playerData;
    public AudioClip damageVoice;
    public AudioClip deadVoice;
    private void Start()
    {
        playerData = playerDataList.Find(stats => stats.playerRank == Progress.Instance.CurrentPlayerRank);
        hp = playerData._hp;
        UpdateStatusUI();
        myLaserNumber = playerData._laserNumber;
        GetComponent<MeshRenderer>().material = playerData.materials;
        damageVoice = playerData.damageVoice;
        deadVoice = playerData.deadVoice;

        laserControllers = GetComponentsInChildren<LaserController>();

        SettingLaser(myLaserNumber);
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            ShotLasers();
            ChangeLasersRotate();
        }
    }

    public void UpdateStatusUI()
    {
        hpText.text = $"{hp}/{playerData._hp}";
        hpGauge.value = hp;
    }

    private void ShotLasers()
    {
        foreach(var laserController in laserControllers)
        {
            StartCoroutine(laserController.ShotLaser());
        }
    }

    private void ChangeLasersRotate()
    {
        foreach(var laserController in laserControllers)
        {
            laserController.ChangeLaserRotate();
        }
    }

    /// <summary>レーザーの回転速度を上げる</summary>
    public void SpeedUpLaserRotate(float magnification)
    {
        for(int i = 0; i < laserControllers.Length; i++)
        {
            var speed = laserControllers[i].rotateSpeed;
            speed *= magnification;
            if(speed > 2)
            {
                speed = 2;
            }
            Debug.Log($"回転速度:{speed}");
        }
    }

    /// <summary>レーザーを射程距離を広げる</summary>
    /// <param name="currentLength"></param>
    public void ExtendLaser()
    {
        for(int i = 0; i < laserControllers.Length; i++)
        {
            laserControllers[i].radius *= 1.5f;
        }
    }

    /// <summary>レーザーを指定数配置</summary>
    public void SettingLaser(uint myLaser)
    {
        //一度全てのレーザーを無効にする
        foreach(var laser in laserControllers)
        {
            laser.gameObject.SetActive(false);
        }

        //指定の数だけ有効
        for(int i = 0; i < myLaser; i++)
        {
            laserControllers[i].gameObject.SetActive(true);

            if(i >= 1)
            {
                //2本目以降は前のレーザーと等間隔に配置
                laserControllers[i].radian = laserControllers[i - 1].radian + (360 / myLaserNumber * (Mathf.PI / 180));
                //回転方向を同じにする
                laserControllers[i].clockwise = laserControllers[0].clockwise;
            }
        }
    }
}

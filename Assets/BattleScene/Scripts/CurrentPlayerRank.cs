﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentPlayerRank : MonoBehaviour
{
    [SerializeField] PlayerData.PlayerRank playerRank;
    private void Start()
    {
        SetCurrentRankText();
    }

    public void SetCurrentRankText()
    {
        if(Progress.Instance.CurrentPlayerRank == playerRank)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}

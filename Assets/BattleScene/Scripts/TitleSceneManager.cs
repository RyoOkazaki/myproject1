﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TitleSceneManager : MonoBehaviour
{
    /// <summary>メニュー画面に遷移するボタン</summary>
    [SerializeField] Button transitionMenuSceneButton = null;
    /// <summary>データリセットメッセージをポップアップするボタン</summary>
    [SerializeField] Button dataResetButton = null;
    /// <summary>データをリセットするボタン</summary>
    [SerializeField] Button submitButton = null;
    /// <summary>データをリセットしないボタン</summary>
    [SerializeField] Button cancelButton = null;
    /// <summary>音量設定ボタン</summary>
    [SerializeField] Button soundSettingButton = null;
    /// <summary>ウィンドウを閉じるボタン</summary>
    [SerializeField] Button closeButton = null;

    /// <summary>データをリセットするか否かを尋ねるウィンドウ</summary>
    [SerializeField] GameObject dataResetWindow = null;
    /// <summary>データをリセットしたことを知らせるウィンドウ</summary>
    [SerializeField] GameObject resetMessage = null;
    /// <summary>音量設定ウィンドウ</summary>
    [SerializeField] GameObject soundSettingWindow = null;

    [SerializeField] Slider BGMSlider = null;
    [SerializeField] Slider SESlider = null;
    [SerializeField] Slider BattleSESlider = null;
    SoundManager soundManager;
    SoundManager.Volume vols;
    private void Awake()
    {
        soundManager = SoundManager.Instance;
    }
    void Start()
    {
        soundSettingWindow.SetActive(false);
        dataResetWindow.SetActive(false);
        resetMessage.SetActive(false);
        transitionMenuSceneButton.onClick.AddListener(() => StartCoroutine(SceneFader.Instance.SceneChange(SceneFader.SceneTitle.Menu,0)));
        dataResetButton.onClick.AddListener(() => PopupWindowController.Popup(dataResetWindow,true));
        submitButton.onClick.AddListener(() => StartCoroutine(DataReset()));
        cancelButton.onClick.AddListener(() => PopupWindowController.Popup(dataResetWindow,false));
        soundSettingButton.onClick.AddListener(() => OpenSoundSettingWindow());
        closeButton.onClick.AddListener(() => CloseSoundSettingWindow());
    }

    /// <summary>設定内容を保存し、ウィンドウを閉じる</summary>
    void CloseSoundSettingWindow()
    {
        soundManager.SaveVolume();
        PlayerPrefs.SetFloat(soundManager.bgmVolumeKey,BGMSlider.value);
        PlayerPrefs.SetFloat(soundManager.seVolumeKey,SESlider.value);
        PlayerPrefs.SetFloat(soundManager.battleSeVolumeKey,BattleSESlider.value);

        PopupWindowController.Popup(soundSettingWindow,false);
    }

    /// <summary>音量設定ウィンドウを開く</summary>
    void OpenSoundSettingWindow()
    {
        //soundManager.LoadVolume();
       // SoundManager.Volume volume = soundManager.CurrentVol;
        PopupWindowController.Popup(soundSettingWindow,true);
        BGMSlider.value = PlayerPrefs.GetFloat(soundManager.bgmVolumeKey);
        SESlider.value = PlayerPrefs.GetFloat(soundManager.seVolumeKey);
        BattleSESlider.value = PlayerPrefs.GetFloat(soundManager.battleSeVolumeKey);
        //BGMSlider.value = volume.volumes[(int)SoundManager.SoundType.BGM];
        //SESlider.value = volume.volumes[(int)SoundManager.SoundType.SE];
        //BattleSESlider.value = volume.volumes[(int)SoundManager.SoundType.BattleSE];
    }

#if UNITY_EDITOR
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.A))//A押したら全クリ
        {
            Progress.Instance.ClearedQuest = QuestData.Difficulty.All;
            Debug.Log(Progress.Instance.ClearedQuest);
        }
    }
#endif

    /// <summary>データをリセットする</summary>
    /// <returns></returns>
    IEnumerator DataReset()
    {
        Progress.Instance.Clear();
        ScoreRanking.Instance.Clear();
        PopupWindowController.Popup(dataResetWindow,false);
        PopupWindowController.Popup(resetMessage,true);
        yield return new WaitForSeconds(3f);
        PopupWindowController.Popup(resetMessage,false);
    }
}

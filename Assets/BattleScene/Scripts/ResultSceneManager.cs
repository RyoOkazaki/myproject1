﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultSceneManager : MonoBehaviour
{
    [SerializeField] List<GameObject> texts = new List<GameObject>();
    [SerializeField] Button button = null;
    Progress progress;
    ScoreRanking scoreRanking;
    private void Awake()
    {
        scoreRanking = ScoreRanking.Instance;
        progress = Progress.Instance;
    }
    void Start()
    {
        button.onClick.AddListener(() => { StartCoroutine(SceneFader.Instance.SceneChange(SceneFader.SceneTitle.Menu,0)); });
        QuestResultData resultData = QuestManager.Instance.GetResultData;
        string rank = "";
        switch(resultData._playerRank)
        {
            case PlayerData.PlayerRank.Low:
                rank = "低級";
                break;
            case PlayerData.PlayerRank.Standard:
                rank = "標準";
                break;
            case PlayerData.PlayerRank.High:
                rank = "高級";
                break;
            case PlayerData.PlayerRank.Special:
                rank = "特級";
                break;
            default:
                break;
        }
        texts[0].GetComponent<Text>().text = rank.ToString();
        texts[1].GetComponent<Text>().text = $"{resultData._playerHp.ToString()}グラム";
        texts[2].GetComponent<Text>().text = $"{resultData._totalScore.ToString()}点";
        texts[3].GetComponent<Text>().text = $"{resultData._totalKillNumber.ToString()}体";
        texts[4].GetComponent<Text>().text = $"{resultData._clearTime.ToString("f0")}秒";

        foreach(var text in texts)
        {
            text.SetActive(false);
        }
        StartCoroutine(ShowResult(0.5f));

        //scoreRanking.SortArray(resultData);
    }

    /// <summary>結果情報を公開する</summary>
    /// <param name="time"></param>
    /// <returns></returns>
    public IEnumerator ShowResult(float time)
    {
        for(int i = 0; i < texts.Count; i++)
        {
            yield return new WaitForSeconds(time);
            texts[i].SetActive(true);
        }
    }
}

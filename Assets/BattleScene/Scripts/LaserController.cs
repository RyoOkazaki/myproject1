﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 線とあたり判定
/// </summary>
public class LaserController : MonoBehaviour
{
    SoundManager soundManager;
    LineRenderer lineRenderer; //ライン
    public float radius = 1.0f; //回転する半径
    public float radian = 0.0f; //角度(ラジアン)
    Vector3 rotatePosition;
    /// <summary>雷エフェクト</summary>
    [SerializeField] GameObject thunderEffect = null;
    /// <summary>時計回りかどうか</summary>
    public bool clockwise = false;
    //2Dでやる場合、Linerendererのuse world spaceはoffること
    [SerializeField] public float rotateSpeed = 1;
    ScoreManager scoreManager;
    AttackComboManager comboManager;
    BattleSceneManager battleSceneManager;
    SpawnManager spawnManager;

    void Awake()
    {
        spawnManager = SpawnManager.Instance;
        battleSceneManager = BattleSceneManager.Instance;
        soundManager = SoundManager.Instance;
        scoreManager = ScoreManager.Instance;
        comboManager = AttackComboManager.Instance;
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.SetPosition(0,transform.position);
    }

    private void LateUpdate()
    {
        if(!clockwise)
        {
            radian += Time.deltaTime * rotateSpeed;
        }
        else
        {
            radian -= Time.deltaTime * rotateSpeed;
        }
        rotatePosition = new Vector3(Mathf.Cos(radian) * radius,Mathf.Sin(radian) * radius,0);
        lineRenderer.SetPosition(1,rotatePosition);
    }

    /// <summary>レーザーを発射！</summary>
    /// <returns></returns>
    public IEnumerator ShotLaser()
    {
        RaycastHit(transform.position + lineRenderer.GetPosition(0),transform.position + lineRenderer.GetPosition(1),radius);
        lineRenderer.material.color = Color.yellow;
        yield return new WaitForSeconds(0.2f);
        lineRenderer.material.color = Color.blue;
    }

    /// <summary>レーザーの回転方向を変える</summary>
    public void ChangeLaserRotate()
    {
        clockwise = !clockwise;
    }

    /// <summary>Rayを発射！</summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    void RaycastHit(Vector3 from,Vector3 to,float distance)
    {
        var hitList = Physics.RaycastAll(from,to - from,distance);
        //何かに当たったとき
        if(hitList.Length > 0)
        {

            foreach(var hit in hitList)
            {
                //エフェクトを発生させる
                GameObject effect = Instantiate(thunderEffect);
                effect.transform.position = hit.point;

                if(hit.collider.tag == "Enemy")
                {
                    var enemyController = hit.collider.GetComponent<EnemyController>();
                    //敵の体力を減らす
                    enemyController.hp--;

                    if(enemyController.hp <= 0)
                    {
                        int addExp = 0;
                        soundManager.PlayWithFade(SoundAsset.BattleSE.EnemyDestroy);
                        switch(enemyController.enemyType)
                        {
                            case EnemyController.EnemyType.Winner:
                                scoreManager.AddScore(EnemyController.EnemyType.Winner);
                                addExp = 10;
                                break;
                            case EnemyController.EnemyType.Steak:
                                scoreManager.AddScore(EnemyController.EnemyType.Steak);
                                addExp = 20;
                                break;
                            case EnemyController.EnemyType.Chicken:
                                scoreManager.AddScore(EnemyController.EnemyType.Chicken);
                                addExp = 30;
                                break;
                            case EnemyController.EnemyType.RawMeat:
                                scoreManager.AddScore(EnemyController.EnemyType.RawMeat);
                                addExp = 80;
                                break;
                            case EnemyController.EnemyType.GoldenBird:
                                scoreManager.AddScore(EnemyController.EnemyType.GoldenBird);
                                addExp = 50;
                                break;
                            default:
                                break;
                        }
                        Destroy(hit.collider.gameObject);
                        comboManager.PopupComboUI(addExp,hit.collider.transform.position);
                        battleSceneManager.totalKillEnemy++;
                    }
                }
                else if(hit.collider.tag == "SpawnPoint")
                {
                    var enemyNest = hit.collider.GetComponent<EnemyGeneratorManager>();
                    enemyNest.hp--;
                    if(enemyNest.hp <= 0)
                    {
                        Destroy(hit.collider.gameObject);
                        spawnManager.currentSpawnCount--;
                        battleSceneManager.CheckClear(spawnManager.currentSpawnCount);
                    }
                }
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class QuestResultData
{
    /// <summary>プレーヤーの価値</summary>
    public PlayerData.PlayerRank _playerRank = PlayerData.PlayerRank.Low;

    /// <summary>プレーヤーの残り体力</summary>
    public int _playerHp;

    /// <summary>スコア</summary>
    public int _totalScore = 0;

    /// <summary>殺した数</summary>
    public int _totalKillNumber = 0;

    /// <summary>クリア時間</summary>
    public float _clearTime = 0;

    public QuestResultData(PlayerData.PlayerRank rank,int hp,int score,int killNumber,float clearTime)
    {
        _playerRank = rank;
        _playerHp = hp;
        _totalScore = score;
        _totalKillNumber = killNumber;
        _clearTime = clearTime;
    }
}
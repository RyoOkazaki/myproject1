﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class ScreenFlash 
{
    /// <summary>画面を赤く点滅させる</summary>
    /// <returns></returns>
    public static IEnumerator DamagedFlashScreen(Image image)
    {
        //StopCoroutine(DamagedFlashScreen());
        image.color = new Color(1,0,0,0.3f);
        yield return new WaitForSeconds(0.2f);
        image.color = new Color(1,0,0,0);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyGeneratorManager : MonoBehaviour
{
    [SerializeField] float animationTime = 3;
    /// <summary>最大体力</summary>
    [SerializeField] int maxHp = 10;
    /// <summary>現在の体力</summary>
    public int hp = 0;
    [SerializeField] List<Vector3> movePath = new List<Vector3>();
    Sequence sequence;

    void Start()
    {
        sequence = DOTween.Sequence();
        hp = maxHp;

        if(movePath.Count > 0)
        {
            Move();
        }
    }

    private void Move()
    {
        animationTime = Random.Range(10f,15f);
        sequence.Append(transform.DOLocalPath(movePath.ToArray(),animationTime));
        sequence.SetLoops(-1,LoopType.Restart);
    }
}
